/*
 * Copyright 2023 Lime Microsystems info@limemicro.com
 * SPDX-License-Identifier: GPL-3.0-or-later
 */
#include "pydoc_macros.h"
#define D(...) DOC(gr,limesdr, __VA_ARGS__ )
/*
  This file contains placeholders for docstrings for the Python bindings.
  Do not edit! These were automatically extracted during the binding process
  and will be overwritten during the build process
 */


 
 static const char *__doc_gr_limesdr_rfe = R"doc()doc";


 static const char *__doc_gr_limesdr_rfe_rfe_0 = R"doc()doc";


 static const char *__doc_gr_limesdr_rfe_rfe_1 = R"doc()doc";


 static const char *__doc_gr_limesdr_rfe_change_mode = R"doc()doc";


 static const char *__doc_gr_limesdr_rfe_set_fan = R"doc()doc";


 static const char *__doc_gr_limesdr_rfe_set_attenuation = R"doc()doc";


 static const char *__doc_gr_limesdr_rfe_set_notch = R"doc()doc";

  
